package jro.com.sslexample;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by jrodriguez on 16/01/16.
 */

public class MainActivity extends AppCompatActivity {
    private TextView tvHtmlCode;
    final Context context = this;
    private String hostname = "";
    private String server = "192.168.0.112";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvHtmlCode = (TextView) findViewById(R.id.tvHtmlCode);
    }


    public void onClick(View view){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_new_album, null);
        final EditText etIp = (EditText) dialogView.findViewById(R.id.editip);
        dialog.setTitle("Introduce your Server IP")
                .setView(dialogView);
        dialog.setCancelable(false)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        hostname = etIp.getText().toString();
                        HttpsCall httpsCall = new HttpsCall(context);
                        httpsCall.execute("https://"+hostname+":8443");
                    }
                });
        final AlertDialog dlg = dialog.create();
        dlg.show();
    }


    public class HttpsCall extends AsyncTask<String, String, String> {

        public Context context;

        public HttpsCall(Context context){
            this.context = context;
        }
        @Override
        public String doInBackground(String... uri) {
            SSLSocketFactoryBuilder sslSocketFactoryBuilder = new SSLSocketFactoryBuilder(org.apache.http.conn.ssl.SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
            final SSLSocketFactory sslSocketFactory =sslSocketFactoryBuilder.getSSLSocketFactory(context,R.raw.keystore,"das2015");

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String host, SSLSession session) {
//                    return host.compareTo(hostname) == 0;
                    HostnameVerifier hv =
                            HttpsURLConnection.getDefaultHostnameVerifier();
                    boolean b = hv.verify(host, session);
                    return b;
                }
            });

            OkHttpClient client = builder.build();
            Request request = new Request.Builder()
                    .url(uri[0])
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                return response.body().string();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result == null){
                tvHtmlCode.setText("Failed to connect");
                tvHtmlCode.setTextColor(context.getResources().getColor(R.color.colorAccent));
            }else{
                tvHtmlCode.setText(result);
                tvHtmlCode.setTextColor(context.getResources().getColor(android.R.color.black));
            }

        }
    }
}
