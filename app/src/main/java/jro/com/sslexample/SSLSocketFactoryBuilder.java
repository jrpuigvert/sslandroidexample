package jro.com.sslexample;

import android.content.Context;
import android.util.Log;

import org.apache.http.conn.ssl.X509HostnameVerifier;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Created by jrodriguez on 16/01/16.
 */

public class SSLSocketFactoryBuilder {
//    implements LayeredSocketFactory

    X509HostnameVerifier hostnameVerifier;

    public  SSLSocketFactoryBuilder(X509HostnameVerifier hostnameVerifier) {
            this.hostnameVerifier = hostnameVerifier;

    }

    /**
     * Create SSLSocketFactory with the given parameters
     * @param context android context
     * @param keystoreResId key store resource id
     * @param keystorePass key store password
     * @return valid SSLSocketFactory instance for given parameters
     */
    public static SSLSocketFactory getSSLSocketFactory(Context context, int keystoreResId, String keystorePass) {
        try {
            // Get an instance of the Bouncy Castle KeyStore format
            KeyStore trusted = KeyStore.getInstance("BKS");
            // Get the raw resource, which contains the keystore with
            // your trusted certificates
            InputStream in = context.getResources().openRawResource(keystoreResId);
            try {
                // Initialize the keystore with the provided trusted certificates
                // Also provide the password of the keystore
                trusted.load(in, keystorePass.toCharArray());
            } finally {
                in.close();
            }

//            MyTrustManager myTrustManager = new MyTrustManager(trusted);
//            TrustManager[] tms = new TrustManager[] { myTrustManager };

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            tmf.init(trusted);

            X509TrustManager xtm = (X509TrustManager) tmf.getTrustManagers()[0];
            for (java.security.cert.X509Certificate cert : xtm.getAcceptedIssuers()) {
                String certStr = "Subject:" + cert.getSubjectDN().getName() + "\nIssuer:"
                        + cert.getIssuerDN().getName();
                Log.d("", certStr);
            }

            SSLContext sslContext = getSslContext();

            sslContext.init(null, tmf.getTrustManagers(),null);
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    private static SSLContext getSslContext() throws NoSuchAlgorithmException {
        final String tlSv1 = "TLSv1";
        final String tlSv12 = "TLSv1.2";
        SSLContext sslContext;
        try {
            // Create a SSLContext with the certificate
            sslContext = SSLContext.getInstance(tlSv12);
        } catch (NoSuchAlgorithmException ex) {
            //fallback, try TLS 1.0, available in Gingerbread
            sslContext = SSLContext.getInstance(tlSv1);
        }
        return sslContext;
    }
//
//    @Override
//    public Socket connectSocket(Socket sock, String host, int port,
//                                InetAddress localAddress, int localPort, HttpParams params)
//            throws IOException, UnknownHostException, ConnectTimeoutException {
//        if (host == null) {
//            throw new IllegalArgumentException("Target host may not be null.");
//        }
//        if (params == null) {
//            throw new IllegalArgumentException("Parameters may not be null.");
//        }
//
//        SSLSocket sslsock = (SSLSocket) ((sock != null) ? sock : createSocket());
//
//        if ((localAddress != null) || (localPort > 0)) {
//            if (localPort < 0)
//                localPort = 0;
//
//            InetSocketAddress isa = new InetSocketAddress(localAddress,
//                    localPort);
//            sslsock.bind(isa);
//        }
//
//        int connTimeout = HttpConnectionParams.getConnectionTimeout(params);
//        int soTimeout = HttpConnectionParams.getSoTimeout(params);
//
//        InetSocketAddress remoteAddress = new InetSocketAddress(host, port);
//
//        sslsock.connect(remoteAddress, connTimeout);
//
//        sslsock.setSoTimeout(soTimeout);
//        try {
//            hostnameVerifier.verify(host, sslsock);
//        } catch (IOException iox) {
//            try {
//                sslsock.close();
//            } catch (Exception x) {
//            }
//
//            throw iox;
//        }
//
//        return sslsock;
//    }
//
//    @Override
//    public Socket createSocket() throws IOException {
//        try {
//            return getSslContext().getSocketFactory().createSocket();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public boolean isSecure(Socket sock) throws IllegalArgumentException {
//        if (sock == null) {
//            throw new IllegalArgumentException("Socket may not be null.");
//        }
//
//        if (!(sock instanceof SSLSocket)) {
//            throw new IllegalArgumentException(
//                    "Socket not created by this factory.");
//        }
//
//        if (sock.isClosed()) {
//            throw new IllegalArgumentException("Socket is closed.");
//        }
//
//        return true;
//
//    }
//
//    @Override
//    public Socket createSocket(Socket socket, String host, int port,
//                               boolean autoClose) throws IOException, UnknownHostException {
//        SSLSocket sslSocket = null;
//        try {
//            sslSocket = (SSLSocket) getSslContext().getSocketFactory().createSocket(socket,
//                    host, port, autoClose);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        hostnameVerifier.verify(host, sslSocket);
//
//        return sslSocket;
//    }

}
